# An Example for the Robot Controller Manager Library

## Overview

This software package provides an example for the C++ robot controller manager [roma](https://bitbucket.org/leggedrobotics/rocoma).

A complete documentation of rocoma is available [here](http://docs.leggedrobotics.com/rocoma_doc/).

The software has been tested under ROS Indigo and Ubuntu 14.04.

The source code is released under a [BSD 3-Clause license](LICENSE).


**Author(s):** Gabriel Hottiger, Christian Gehring


## Building

[![Build Status](https://ci.leggedrobotics.com/buildStatus/icon?job=bitbucket_leggedrobotics/rocoma_example/master)](https://ci.leggedrobotics.com/job/bitbucket_leggedrobotics/job/rocoma_example/job/master/)

In order to install, clone the latest version from this repository into your catkin workspace and compile the packages.

### Dependencies

* **[rocoma](https://bitbucket.org/leggedrobotics/rocoma):** Robot controller manager library
	

## Bugs & Feature Requests

Please report bugs and request features using the [Issue Tracker](https://github.com/ethz-asl/ros_best_practices/issues).